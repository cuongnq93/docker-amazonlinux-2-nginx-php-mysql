FROM amazonlinux:2

RUN yum clean all
RUN yum -y update

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum install -y epel-release

RUN yum install -y php72 php72-fpm nginx

EXPOSE 8080 8100

CMD (tail -F /var/log/nginx/access.log &) && exec nginx -g "daemon off;"